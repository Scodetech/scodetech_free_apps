# -*- encoding: utf-8 -*-
############################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010 Zikzakmedia S.L. (<http://www.zikzakmedia.com>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##########################################################################

from odoo import models, fields, api, _
from odoo.exceptions import UserError

from ftplib import FTP


class product_icecat(models.Model):
    _name = "product.icecat"
    _description = "Icecat Configuration"

    @api.multi
    def check_ftp(self):
            try:
                ftp = FTP(self.ftpip)
            except:
                raise UserError(_('Error : IP FTP connection was not successfully!'))
            try:
                ftp.login(self.ftpusername, self.ftppassword)
            except:
                raise UserError(_('Error : Username/password FTP connection was not successfully!'))

            ftp.quit()
            raise UserError(_('Ok ! : FTP connection was successfully!'))

    name = fields.Char('Name', size=32, required=True)
    username = fields.Char('User Name', size=32, required=True)
    password = fields.Char('Password', size=32, required=True)
    active = fields.Boolean('Active', default=lambda *a: 1)

    @api.model
    def create(self, vals):
        if vals.get('active', False):
            actv_ids = self.search([('active', '=', True)])
            if len(actv_ids):
                raise UserError(_('Error : They are other icecat configuration with "Active" field checked. Only one configuration is avaible for active field.'))

        return super(product_icecat, self).create(vals)
