# -*- encoding: utf-8 -*-
############################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010 Zikzakmedia S.L. (<http://www.zikzakmedia.com>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##########################################################################

from odoo import models, fields, api, _
import os
import re
import libxml2
import urllib2
from urllib2 import URLError
from ftplib import FTP
from odoo.exceptions import UserError
import base64, urllib


class product_icecat_wizard(models.TransientModel):
    _name = 'product.icecat.wizard'

    manufacturer_code = fields.Char("Product Code", required=True)
    brand = fields.Char("Brand", required=True)
    language_id = fields.Many2one('res.lang', 'Language')

    # ==========================================
    # save XML file into product_icecat/xml dir
    # ==========================================
    def save_file(self, name, value):
        path = os.path.abspath(os.path.dirname(__file__))
        path += '/icecat/%s' % name
        path = re.sub('wizard/', '', path)
        path = '/tmp/%s' % name
        f = open(path, 'w+')
        try:
            f.write(value)
        finally:
            f.close()
        return path

    # ==========================================
    # Convert HTML to text
    # ==========================================
    def StripTags(self, text):
        finished = 0
        while not finished:
            finished = 1
            start = text.find("<")
            if start >= 0:
                stop = text[start:].find(">")
                if stop >= 0:
                    text = text[:start] + text[start + stop + 1:]
                    finished = 0
        return text

    # ==========================================
    # Convert icecat values to OpenERP mapline
    # ==========================================
    def icecat2oerp(self, form, product, icecat, pathxml, language,
                    data, context):

        if form.language_id.code:
            language = form.language_id.code

        doc = libxml2.parseFile(pathxml)

        for prod in doc.xpathEval('//Product'):
            if prod.xpathEval('@ErrorMessage'):
                if prod.xpathEval('@ErrorMessage')[0].content:
                    raise UserError(_('Error : %s') % (prod.xpathEval('@ErrorMessage')[0].content))

        # product info
        short_summary = doc.xpathEval('//SummaryDescription//ShortSummaryDescription')
        long_summary = doc.xpathEval('//SummaryDescription//LongSummaryDescription')

        short_description = short_summary[0].content
        description = long_summary[0].content
        name = description.split('.')[0]

        # manufacturer product
        manufacturers = ''
        for supplier in doc.xpathEval('//Supplier'):
            manufacturers = supplier.xpathEval('@Name')[0].content
        partner_id = False
        if len(manufacturers) > 0:
            partner_id = self.env['res.partner'].search([('name', 'ilike', manufacturers)])

        ref = ''
        for prod in doc.xpathEval('//Product'):
            ref = prod.xpathEval('@Prod_id')[0].content
            break

        category = doc.xpathEval('//Category//Name//@Value')[0].content

        barcode = ''
        for ean in doc.xpathEval('//EANCode//@EAN'):
            barcode = ean.xpathEval('//@EAN')[0].content
        url = ''
        for u in doc.xpathEval('//ProductDescription//@URL'):
            url = u.xpathEval('//@URL')[0].content

        high_pic = ''
        if prod.xpathEval('@HighPic'):
            high_pic = prod.xpathEval('@HighPic')[0].content
        low_pic = ''
        if prod.xpathEval('@LowPic'):
            low_pic = prod.xpathEval('@LowPic')[0].content
        thumb_pic = ''
        if prod.xpathEval('@ThumbPic'):
            thumb_pic = prod.xpathEval('@ThumbPic')[0].content
        prod.xpathEval('//Product//@Prod_id')[0].content
        (filename, header) = urllib.urlretrieve(thumb_pic)
        f = open(filename, 'rb')
        img = base64.encodestring(f.read())

        #create product
        vals = {
            'name': name,
            'description_sale': short_description,
            'description_purchase': description,
            'manufacturer': partner_id and partner_id.id or False,
            'manufacturer_pname': name,
            'manufacturer_pref': ref,
            'manufacturer_purl': url,
            'barcode': barcode,
            'default_code': ref,
            'invoice_policy': 'delivery',
            'image_medium': img
        }
        product_id = self.env['product.product'].create(vals)
        high_img = False
        low_img = False
        if high_pic:
            (filename, header) = urllib.urlretrieve(high_pic)
            f = open(filename, 'rb')
            high_img = base64.encodestring(f.read())
        if low_pic:
            (filename, header) = urllib.urlretrieve(low_pic)
            f = open(filename, 'rb')
            low_img = base64.encodestring(f.read())
        if high_pic:
            self.env['product.images'].create({'name': 'HighPic', 'link': True, 'url': high_pic, 'product_id': product_id.id, 'file_db_store': high_img, 'file': high_img})
        if low_pic:
            self.env['product.images'].create({'name': 'LowPic', 'link': True, 'url': low_pic, 'product_id': product_id.id, 'file_db_store': low_img, 'file': low_img})
        if thumb_pic:
            self.env['product.images'].create({'name': 'ThumbPic', 'link': True, 'url': thumb_pic, 'product_id': product_id.id, 'file_db_store': img, 'file': img})

        result = _("Product %s XML Import successfully") % name

        return result

    # ==========================================
    # Convert icecat values to OpenERP mapline
    # ==========================================
    def iceimg2oerpimg(self, cr, uid, form, product, icecat, pathxml, data,
                       context):
        doc = libxml2.parseFile(pathxml)

        # product image
        for prod in doc.xpathEval('//Product'):
            if prod.xpathEval('@HighPic'):
                image = prod.xpathEval('@HighPic')[0].content

        if image:
            fname = image.split('/')
            fname = fname[len(fname) - 1]

            path = os.path.abspath(os.path.dirname(__file__))
            path += '/icecat/%s' % fname
            path = re.sub('wizard/', '', path)

            # download image
            urllib.urlretrieve(image, path)

            # send ftp server
            ftp = FTP(icecat.ftpip)
            ftp.login(icecat.ftpusername, icecat.ftppassword)
            ftp.cwd(icecat.ftpdirectory)
            f = file(path, 'rb')
            ftp.storbinary('STOR ' + os.path.basename(path), f)
            ftp.quit()

            # add values into product_image
            # product info
            long_summary = doc.xpathEval(
                '//SummaryDescription//LongSummaryDescription')
            description = long_summary[0].content
            name = description.split('.')[0]

            values = {
                'name': name,
                'link': 1,
                'filename': icecat.ftpurl + fname,
                'product_id': product.id,
            }
            self.pool.get('product.images').create(cr, uid, values, context)
            return icecat.ftpurl + fname
        else:
            return _("Not exist %s image") % fname

    # ==========================================
    # wizard
    # =========================================
    @api.multi
    def import_xml(self):
        icecat_id = self.env['product.icecat'].search([('active', '=', 1)])
        if not icecat_id:
            raise UserError(_('Error : Configure your icecat preferences!'))

        icecat = icecat_id[0]

        form = self.browse(self._ids[0])

        if not form.language_id:
            language = self.env['res.users'].browse(self._uid).lang
            lang = language.split('_')[0]
        else:
            language = form.language_id.code
            lang = language.split('_')[0]

        context = dict(self._context or {})
        product = self.env['product.product'].search([('manufacturer_pref', '=', self.manufacturer_code)])
        if not product:
            url = 'https://data.icecat.biz/xml_s3/xml_server3.cgi?prod_id=%s;brand=%s;lang=%s;output=productxml' % (self.manufacturer_code, self.brand, lang)
            fileName = '%s.xml' % self.manufacturer_code

            passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
            # this creates a password manager
            passman.add_password(
                None, url, icecat.username, icecat.password)

            authhandler = urllib2.HTTPBasicAuthHandler(passman)
            # create the AuthHandler

            openerp = urllib2.build_opener(authhandler)

            urllib2.install_opener(openerp)
            # All calls to urllib2.urlopen will now use our handler

            try:
                urllib2.urlopen(url)
                req = urllib2.Request(url)
                handle = urllib2.urlopen(req)
                content = handle.read()
                # save file
                pathxml = self.save_file(fileName, content)
                data = []
                result = self.icecat2oerp(form, product, icecat, pathxml, language, data, context)
            except URLError, e:
                result = e.code

        else:
            raise UserError(_('Warning : This Product is already in odoo.'))
        return True
