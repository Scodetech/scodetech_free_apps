# coding: utf-8
#########################################################################
# Copyright (C) 2009  Sharoon Thomas, Open Labs Business solutions      #
#                                                                       #
# This program is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by   #
# the Free Software Foundation, either version 3 of the License, or      #
#(at your option) any later version.                                    #
#                                                                       #
# This program is distributed in the hope that it will be useful,        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          #
# GNU General Public License for more details.                           #
#                                                                       #
# You should have received a copy of the GNU General Public License      #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.  #
#########################################################################
from odoo import models, fields, api
import os

class product_images(models.Model):
    _name = "product.images"

    name = fields.Char(string='Image Title', size=100, required=True)
    link = fields.Boolean(string='Link?', help="Images can be linked from files on your file system or remote (Preferred)")
    file = fields.Binary(string="File")
    url = fields.Char(string='File Location', size=250)
    comments = fields.Text(string='Comments')
    product_id = fields.Many2one('product.product', string='Product')

    extention = fields.Char(string='file extention', size=6)
    file_db_store = fields.Binary(string='Image stored in database')

    @api.model
    def create(self, vals):
        if vals.get('name', False) and not vals.get('extention', False):
            vals['name'], vals['extention'] = os.path.splitext(vals['name'])
        return super(product_images, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('name', False) and not vals.get('extention', False):
            vals['name'], vals['extention'] = os.path.splitext(vals['name'])
        if vals.get('name', False) or vals.get('extention', False):
            local_media_repository = self.env['res.company'].get_local_media_repository()
            if local_media_repository:
                old_images = self
                res = []
                for old_image in old_images:
                    if vals.get('name', False) and (old_image.name != vals['name']) or vals.get('extention', False) and (old_image.extention != vals['extention']):
                        old_path = os.path.join(local_media_repository, old_image.product_id.default_code, '%s%s' % (old_image.name, old_image.extention))
                        res.append(super(product_images, self).write(vals))
                        if 'file' in vals:
                            #a new image have been loaded we should remove the old image
                            #TODO it's look like there is something wrong with function field in openerp indeed the preview is always added in the write :(
                            if os.path.isfile(old_path):
                                os.remove(old_path)
                        else:
                            #we have to rename the image on the file system
                            if os.path.isfile(old_path):
                                os.rename(old_path, os.path.join(local_media_repository, old_image.product_id.default_code, '%s%s' % (old_image.name, old_image.extention)))
                return res
        return super(product_images, self).write(vals)
